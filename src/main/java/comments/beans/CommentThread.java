package comments.beans;
public class CommentThread {
    private String id;

    public CommentThread(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
