package comments.beans;

public class Post {
    private String id;
    private String comment;
    private long date;
    private String threadId;

    public Post(String id, String threadId, String comment, long date) {
        this.setThreadId(threadId);
        setId(id);
        setComment(comment);
        setDate(date);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }
}
