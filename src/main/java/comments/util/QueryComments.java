package comments.util;

import java.util.Iterator;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.view.Stale;
import com.couchbase.client.java.view.ViewQuery;
import com.couchbase.client.java.view.ViewResult;
import com.couchbase.client.java.view.ViewRow;

public class QueryComments {
    public static void main(String[] args) {
        CouchbaseCluster cluster = CouchbaseCluster.create("127.0.0.1");
        Bucket bucket = cluster.openBucket("comments");

        String threadId = "thread_0";
        JsonArray endKey = JsonArray.from(threadId, "\uefff");
        JsonArray startKey = JsonArray.from(threadId, "");
        ViewQuery query = ViewQuery.from("comments", "by_thread").limit(100).skip(0).descending(false).startKey(startKey).endKey(endKey).inclusiveEnd(false).stale(Stale.UPDATE_AFTER);

        long start = System.currentTimeMillis();
        {
            for (int i = 0; i < 1000; i++) {
                bucket.query(query);
            }
        }
        System.out.println((System.currentTimeMillis() - start) + "ms");

        ViewResult result = bucket.query(query);
        Iterator<ViewRow> rows = result.rows();
        while (rows.hasNext()) {
            ViewRow row = rows.next();
            System.out.println(row.document().id());
        }
        System.out.println(result.allRows().size());
    }
}
