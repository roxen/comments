package comments.util;

import java.util.Arrays;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.view.DefaultView;
import com.couchbase.client.java.view.DesignDocument;
import com.couchbase.client.java.view.View;
import com.google.gson.Gson;
import comments.beans.CommentThread;
import comments.beans.Post;

public class CreateComments {
    private static final String DEV_COMMENTS = "dev_comments";
    private static final String BY_THREAD = "by_thread";

    public static void main(String[] args) {
        CouchbaseCluster cluster = CouchbaseCluster.create("127.0.0.1");
        Bucket bucket = cluster.openBucket("comments");

        View byThreadView = DefaultView
                .create(BY_THREAD,
                        "function (doc, meta) { if (doc.threadId) { var padding = '00000000000000'; var timestamp = padding + doc.date; timestamp = timestamp.substr(timestamp.length - padding.length); emit([doc.threadId, timestamp], null);}}");
        DesignDocument commentsDesignDoc = DesignDocument.create(DEV_COMMENTS, Arrays.asList(byThreadView));
        bucket.bucketManager().removeDesignDocument(DEV_COMMENTS);
        bucket.bucketManager().insertDesignDocument(commentsDesignDoc);

        int nofThreads = 5;
        int nofCommentsPerThread = 1000;
        long start = System.currentTimeMillis();
        for (int i = 0; i < nofThreads; i++) {
            CommentThread commentThread = new CommentThread("thread_" + i);
            bucket.async().upsert(RawJsonDocument.create(commentThread.getId(), new Gson().toJson(commentThread)));
            for (int j = 0; j < nofCommentsPerThread; j++) {
                Post post = new Post("post_" + i + "_" + j, commentThread.getId(), String.format("This is comment no %d.", j), j);
                bucket.async().upsert(RawJsonDocument.create(post.getId(), new Gson().toJson(post)));
            }
        }

        System.out.println("created " + (nofThreads * nofCommentsPerThread) + " comments in " + (System.currentTimeMillis() - start) + " ms");

        cluster.disconnect();
    }
}
