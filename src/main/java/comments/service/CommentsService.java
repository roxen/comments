package comments.service;

import java.util.Iterator;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.view.Stale;
import com.couchbase.client.java.view.ViewQuery;
import com.couchbase.client.java.view.ViewResult;
import com.couchbase.client.java.view.ViewRow;

@Path("thread")
public class CommentsService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{threadId}")
    public Response getComments(@PathParam("threadId") String threadId, @DefaultValue("100") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        CouchbaseCluster cluster = CouchbaseCluster.create("127.0.0.1");
        Bucket bucket = cluster.openBucket("comments");

        JsonObject response = JsonObject.create();
        JsonDocument thread = bucket.get(threadId);
        if (thread != null) {
            response.put("thread", thread.content());

            JsonArray endKey = JsonArray.from(threadId, "\uefff");
            JsonArray startKey = JsonArray.from(threadId, "");
            if (descending) {
                JsonArray tmp = endKey;
                endKey = startKey;
                startKey = tmp;
            }

            ViewQuery query = ViewQuery.from("comments", "by_thread").limit(limit).skip(skip).descending(descending).startKey(startKey).endKey(endKey).inclusiveEnd(false).stale(Stale.FALSE);
            ViewResult result = bucket.query(query);
            System.out.println(query.toString());
            Iterator<ViewRow> rows = result.rows();
            JsonArray comments = JsonArray.create();
            while (rows.hasNext()) {
                ViewRow row = rows.next();
                JsonDocument document = row.document();
                comments.add(document.content());
            }
            response.put("comments", comments);
        }

        return Response.ok(response.toString()).build();
    }
}
